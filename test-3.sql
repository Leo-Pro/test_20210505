CREATE DATABASE data_db;
USE data_db;
CREATE TABLE company (
     id MEDIUMINT NOT NULL AUTO_INCREMENT,
     name CHAR(250),
     PRIMARY KEY (id)
);
CREATE TABLE city (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    name CHAR(50),
    PRIMARY KEY (id)
);
CREATE TABLE office (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    name CHAR(250),
    PRIMARY KEY (id),
    companyId MEDIUMINT,
    FOREIGN KEY (companyId)  REFERENCES company (Id),
    cityId MEDIUMINT,
    FOREIGN KEY (cityId)  REFERENCES city (Id)

);
----------------------------------------
SELECT name, companyId  FROM `office`
WHERE companyId = @companyId;
----------------------------------------
SELECT companyId, cityId  FROM `office`
WHERE cityId = @cityId;
----------------------------------------
SELECT name, cityId  FROM `office`
WHERE cityId = @cityId;
----------------------------------------