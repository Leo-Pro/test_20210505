"use strict";
document.getElementById("value").onclick =  () => {
    let num1 = +document.getElementById("num1").value;
    let num2 = +document.getElementById("num2").value;
    if (Number(num1) && Number(num2)) {
        document.getElementById("result").innerHTML = num1 + num2;
    } else {
        document.getElementById("result").innerHTML = 'Вы ввели не число';
    }
};
