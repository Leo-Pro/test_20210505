<?php
    $tree = array (
        "level 1" => array("level 1.1", "level 1.2"),
        "level 2",
        "level 3" => array("level 3.1", "level 3.2" => array("level 3.2.1", "level 3.2.2"), "level 3.3"),
        "level 4" => array("level 4.1", "level 4.2", "level 4.3", "level 4.4"),
    );
    function printArray($data){
        foreach($data as $item) {
            if(is_string($item)){
                echo str_repeat('&#160;&#160;', count($data) -1) .$item .'<br>';
            } else {
                printArray($item);
            }
        }
    }
    printArray($tree);