<?php
function declination($quantity) {
    if (($quantity%100 > 4 && $quantity%100 < 20) || $quantity%10 === 1) {
        return $quantity .' Программист <br>';
    } elseif ($quantity%10 > 1 && $quantity%10 < 5) {
        return $quantity .' Программиста  <br>';
    } elseif($quantity%10 === 0 || $quantity%10 > 4) {
        return $quantity .' Программистов  <br>';
    }
    return; 
};

echo declination(0);
echo declination(128);
echo declination(101);
echo declination(2);
echo declination(3);
